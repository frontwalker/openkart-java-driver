## Fredriks gulasch till ca 50 pers eller 30 studenter

* 6 kg högrev i bitar 

Bryn och lägg åt sidan.

* 2,5 kg hackad lök
* 4 hackade vitlökar

Fräs i smör tills gyllene.

* 5 dl ungersk söt paprika (pulver)
* 1 dl hel kummin
* några stjärnanisar
* 800 g tomatpuré

Tillsätt kryddor och tomatpuré och stek försiktigt med löken några minuter. Försiktigt så att inte paprikan bränns. I med köttet.

* buljong (jag änvände 2 l köttglace) + ev vatten till lagom konsistens.

Koka tills köttet är mört (1-2h). Potatis och ev morot kan tillsättas när det börjar bli färdigt

* svartpeppar
* cayennepeppar
* salt

Efter behag.